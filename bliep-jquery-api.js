(function (factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else {
        factory(jQuery);
    }
}(function ($) {
    // default storage; in-memory browser session
    var APISessionStorage = {
      data: {},
      read: function(key) { return this.data[key]; },
      write: function(key, value, expires) { this.data[key] = value; },
      delete: function(key) { this.data[key] = null; }
    };

    // cookie-based storage; depends on jQuery Cookie Plugin
    var APICookieStorage = {
      apiKeys: ['accessToken', 'clientAccessToken', 'refreshToken'],
      data: {},
      expires: null,
      read: function(key) { return $.cookie(key); },
      write: function(key, value, expires_in_days) {
        if (expires_in_days) this.expires = expires_in_days // cache expiration preference
        var opts = {expires: this.expires, path: '/', secure: true };

        $.cookie(key, value, opts);
        this.data[key] = value;
        return {key: key, value: value, options: opts};
      },
       delete: function(key) {
         // needed for local dev
         $.removeCookie(key);
         // needed on prod
         $.removeCookie(key, { path: '/' });
         // needed for helpdesk
         $.removeCookie(key, {path: "/", domain: ".bliep.nl"});
         delete this.data[key];
       }
    };

    // storage factory. TODO: add local storage
    var APIStorage = function() {
        if (typeof $.cookie === 'function') {
          return APICookieStorage;
        } else {
          return APISessionStorage;
        }
    };

    // API HTTP Client
    function BliepClient (options) {
        options = options || {};
        this.settings = $.extend({}, {
            clientId : options.clientId,
            baseUrl  : options.baseUrl || "https://api.bliep.nl/api/",
            expires  : null,
            debug    : false,
            test     : false,
            done     : null,
            fail     : null
        }, options);

        this.storage = APIStorage();
        this.accessToken = this.storage.read('accessToken') || '';
        this.clientAccessToken = this.storage.read('clientAccessToken') || '';
        this.ajaxOptions = {}; // jquery ajax options: http://api.jquery.com/jquery.ajax/
        this.ready = new $.Deferred();

        this.log("Initialized BliepClient with settings: "+JSON.stringify(this.settings));

        // immediately authenticate the client if no token exists and the 'authenticate' flag was set
        if (!this.clientAccessToken && this.settings.authenticate === true) {
            this.log("No client token, authenticating");
            this.authenticateClient();
        } else if (this.isClientAuthenticated()) {
            this.log("Client initialized with stored token: "+this.clientAccessToken);
            this.ready.resolve(true);
        }

        return this;
    }
    BliepClient.APICookieStorage = APICookieStorage;
    BliepClient.APISessionStorage = APISessionStorage;

    BliepClient.prototype.log = function(msg) {
        if (this.settings.debug) {
            console.log(msg);
        }
    };
    BliepClient.prototype.isUserAuthenticated = function() {
        return this.accessToken ? true : false;
    };
    BliepClient.prototype.isClientAuthenticated = function() {
        return this.clientAccessToken ? true : false;
    };
    BliepClient.prototype.authenticateClient = function() {
        var self = this;
        return this.post( "oauth2/token", {
                client_id: this.settings.clientId,
                grant_type: 'implicit',
                response_type: 'token'
            })
            .done(function(data) {
                self.clientAccessToken = data.access_token;
                self.storage.write('clientAccessToken', data.access_token, self.settings.expires);
                self.ready.resolve(true);
            });
    };
    BliepClient.prototype.requestOptions = function(data) {
        var self = this;
        var options = {
            contentType: "application/json",
            dataType: "json",
            data: data,
            headers: {
                'Authorization' : 'Bearer '+ (this.accessToken || this.clientAccessToken),
                'X-Requested-With': 'XMLHttpRequest'
            },
            beforeSend: function(){ self.beforeRequest(); },
            complete: function(){ self.afterRequest(); } };
        return $.extend( {}, options, this.ajaxOptions);
    };
    BliepClient.prototype.beforeRequest = function(jqXHR, settings) {};
    BliepClient.prototype.request = function(options) {
      options = options || { type: '', url: '' };
      var req = $.ajax(options);
      var def = new $.Deferred();
      var self = this;

      // optionally log request and response data for debugging
      if (this.settings.debug) {
        this.log(options.type+' ('+options.url+') JSON: '+JSON.stringify(options.data));

        var logResponse = function(data){ self.log('RESPONSE ('+options.url+') JSON '+JSON.stringify(data)); };
        req.always( logResponse );
      }

      // bind callback hooks
      req.fail( function(data) {
        if (typeof self.settings.fail === 'function') {
          self.settings.fail(data, options);
        }
        def.reject(data);
      })
      .done( function(data) {
        if (data.status === "error" || data.status === "fail") {
          if (typeof self.settings.fail === 'function') {
            self.settings.fail(data, options);
          }
          def.reject(data);
        } else {
          if (typeof self.settings.done === 'function') {
            self.settings.done(data, options);
          }
          def.resolve(data);
        }
      });

      return def;
    };
    BliepClient.prototype.afterRequest = function(jqXHR) {};
    BliepClient.prototype.mockRequest = function(method, path, data) {
        if (this.settings.debug) {
            this.log(method+' '+path+' :: '+JSON.stringify(data));
        }
        var req = new $.Deferred();
        data = $.MockData[path] && $.MockData[path][method] ? $.MockData[path][method] : {};
        var res = {status: 'success', data: data};

        // mock oauth requests by persisting fake token
        if (data.access_token) {
            self.accessToken = data.access_token;
            this.storage.write('accessToken', data.access_token, this.settings.expires);
        }

        return req.resolve(res);
    };
    BliepClient.prototype.get = function(path, data) {
        if (this.settings.test) return this.mockRequest('get', path, data);

        var url = this.settings.baseUrl+path;
        var options = $.extend( this.requestOptions(data), {type: "GET", url: url});

        return this.request(options);
    };
    BliepClient.prototype.post = function(path, data) {
        if (this.settings.test) return this.mockRequest('post', path, data);

        var url = this.settings.baseUrl+path;
        var options = $.extend( this.requestOptions(JSON.stringify(data)), {type: "POST", url: url});

        return this.request(options);
    };
    BliepClient.prototype.put = function(path, data) {
        if (this.settings.test) return this.mockRequest('put', path, data);

        var url = this.settings.baseUrl+path;
        var options = $.extend( this.requestOptions(JSON.stringify(data)), {type: "PUT", url: url});

        return this.request(options);
    };
    BliepClient.prototype.delete = function(path, data) {
        if (this.settings.test) return this.mockRequest('delete', path, data);

        var url = this.settings.baseUrl+path;
        var options = $.extend( this.requestOptions(data), {type: "DELETE", url: url});

        return this.request(options);
    };


    // implementation
    BliepClient.prototype.authenticate = function(username, password) {
        var self = this;
        return this.post( "authenticate", { username: username, password: password, client_id: this.settings.clientId })
            .done(function(data) {
                self.accessToken = data.access_token;
                self.storage.write('accessToken', data.access_token, self.settings.expires);
                self.storage.write('refreshToken', data.refresh_token, self.settings.expires);
                self.ready.resolve(true);
            });
    };
    BliepClient.prototype.registerAccount = function(name, email, password, sex, msisdn, birthdate) {
        return this.post( "registration", { name: name, password: password, email: email, sex: sex, msisdn: msisdn, birthdate: birthdate });
    };
    BliepClient.prototype.confirmAccount = function(code, msisdn) {
        return this.put( "registration", { confirmcode: code, msisdn: msisdn });
    };
    BliepClient.prototype.orderAndRegister = function(name, email, password, sex, birthdate, number_porting, street, house_number, house_number_extension, city, postal_code, sim_type, campaign_id, redirect_url, referral_code) {
        return this.post( "orders", {
            name: name,
            password: password,
            email: email,
            sex: sex,
            birthdate: birthdate,
            numberporting: number_porting,
            cityname: city,
            housenumber: house_number,
            housenumberext: house_number_extension,
            streetname: street,
            postalcode: postal_code,
            simtype: sim_type,
            campaign_id: campaign_id,
            redirect_url: redirect_url,
            referral_link: referral_code,
            payment_method: 'ideal' });
    };
    BliepClient.prototype.updateSIMCardState = function(bliep_state, bliep_plus, bliep_voice) {
        return this.put( "profile/state", { on: bliep_state, plus: bliep_plus, voice: bliep_voice } );
    };
    BliepClient.prototype.listPortingProviders = function() {
        return this.get( "porting/providers", null );
    };
    BliepClient.prototype.createPortIn = function(provider, iccid, msisdn, contractType, orderId, orderSecret) {
        return this.post( "porting",
          { provider: provider, iccid: iccid, msisdn: msisdn, contract_type: contractType, orderid: orderId, ordersecret: orderSecret }
        );
    };
    BliepClient.prototype.getProviderSimFormat = function(provider_code) {
        return this.get( "porting/simformat",
          { provider_code: provider_code }
        );
    };
    BliepClient.prototype.checkProviderSimFormat = function(provider_code, iccid) {
        return this.get( "porting/simformat/check",
          { provider_code: provider_code, iccid: iccid }
        );
    };
    BliepClient.prototype.listCreditHistory = function(offset, limit) {
        return this.get( "profile/credit/history", { limit: limit, offset: offset } );
    };
    BliepClient.prototype.shareCredit = function(amount, msisdn, addressbook, personalMessage) {
        return this.post( "profile/credit/share",
          { amount: amount, msisdn: msisdn, addressbook: addressbook, personal_message: personalMessage }
        );
    };
    BliepClient.prototype.listSharingAddressBook = function() {
        return this.get( "profile/credit/share/addressbook" );
    };
    BliepClient.prototype.getRecurringPaymentSettings = function() {
        return this.get( "profile/settings/recurring_payment" );
    };
    BliepClient.prototype.setRecurringPaymentSettings = function(enableMonthly, monthlyAmount, enableLowCredit, lowCreditAmount) {
        return this.put( "profile/settings/recurring_payment", {
            monthly: { enabled: enableMonthly, amount: monthlyAmount},
            lowcredit: { enabled: enableLowCredit, amount: lowCreditAmount}
        });
    };
    BliepClient.prototype.disableRecurringContract = function() {
        return this.delete( "profile/settings/recurring_payment");
    };
    BliepClient.prototype.topupCredit = function(amount, redirect_url, recurring) {
        return this.post( "profile/credit/web", { amount: amount, redirect_url: redirect_url, recurring: recurring } );
    };
    BliepClient.prototype.topupVoucherCredit = function(voucherCode) {
        return this.post( "profile/credit/voucher", { voucher_code: voucherCode } );
    };
    BliepClient.prototype.listCallHistory = function(offset, limit) {
        return this.get( "profile/call/history", { limit: limit, offset: offset } );
    };
    BliepClient.prototype.viewSettings = function() {
        return this.get( "profile/settings" );
    };
    BliepClient.prototype.updateCredentials = function(email, password, new_password) {
        return this.put( "profile",
          { email: email, password: password, new_password: new_password }
        );
    };
    BliepClient.prototype.updateSettings = function(callFromBalance, voicemailActive) {
        return this.put( "profile/settings",
          { call_from_balance: callFromBalance, voicemail_active: voicemailActive }
        );
    };
    BliepClient.prototype.viewRoamingSettings = function() {
        return this.get( "profile/settings/roaming" );
    };
    BliepClient.prototype.updateRoamingSettings = function(settings) {
        return this.put( "profile/settings/roaming", settings);
    };
    BliepClient.prototype.viewProfile = function() {
        return this.get( "profile" );
    };
    BliepClient.prototype.forgotLogin = function(email, msisdn) {
        return this.post( "profile/forgot_login", { email: email, msisdn: msisdn} );
    };
    BliepClient.prototype.viewReferral = function() {
        return this.get( "profile/referral" );
    };
    BliepClient.prototype.viewSignature = function() {
        return this.get( "profile/signature" );
    };
    BliepClient.prototype.updateSignature = function(signature, use_signature) {
        return this.put( "profile/signature", { signature: signature, use: use_signature });
    };
    BliepClient.prototype.listFAQ = function(category_id) {
        var params = category_id ? { category: category_id } : {};
        return this.get( "help/faq", params);
    };
    BliepClient.prototype.searchFAQ = function(text) {
        return this.get( "help/search", { text: text } );
    };
    BliepClient.prototype.askQuestion = function(name, email, message) {
        return this.post( "help/faq", { name: name, email: email, message: message } );
    };
    BliepClient.prototype.logout = function() {
        this.accessToken = null;
        this.storage.delete('accessToken');
        this.storage.delete('refreshToken');
    };
    BliepClient.prototype.checkEmail = function(email) {
        return this.get( "check_email", { email: email } );
    };
    BliepClient.prototype.geolocateAddress = function(postalcode, housenumber) {
        return this.get( "check_postcode", { postalcode: postalcode, housenumber: housenumber } );
    };


    // **************************************
    // Mock data used returned when 'test' option set on client. mocks api network requests
    // **************************************
    $.MockData = {
        "oauth2/token": {
            "post": {
                "access_token": "34t3t4334gq3gg3gqqgq",
                "refresh_token": "tq2f9f0jvtn3nq3049qn"
            }

        },
        "authenticate": {
            "post": {
                "access_token": "34t3t4334gq3gg3gqqgq",
                "refresh_token": "tq2f9f0jvtn3nq3049qn"
            }
        },
        "registration": {
            "post": {},
            "put": {
                "access_token": "34t3t4334gq3gg3gqqgq",
                "refresh_token": "tq2f9f0jvtn3nq3049qn"
            }
        },
        "orders": {
            "post": {
                "redirect_url": "https://test.adyen.com/hpp/pay.shtml?",
                "orderid": "6368371348078592",
                "ordersecret": "a51f3234b23fc9d0a6fa9ec8a355aeed653d1862"
            }
        },
        "profile/state": { "put": {} },
        "porting/providers": {
            "get": {
              "is_port_in_allowed": true,
              "main_providers": [{"code": "BEN-BEN", "name": "T-Mobile"}, {"code": "LTEL-LIFN", "name": "Vodafone Abonnement"}, {"code": "LTEL-LBTP", "name": "Vodafone Prepaid"}, {"code": "GSM1-SPM", "name": "KPN"}, {"code": "GSM1-SPM", "name": "Hi"}],
              "providers": [{"code": "INMO-INMO", "name": "6GMOBILE"}, {"code": "ETMB-ACHT", "name": "888 BV"}, {"code": "TLFM-ACNM", "name": "ACN Mobile"}, {"code": "TLFM-BING", "name": "AH Mobiel"}, {"code": "TLFM-MECS", "name": "Amatus"}, {"code": "BEN-ARTA", "name": "Artilium Mobile"}, {"code": "GSM1-ASPI", "name": "Aspider"}, {"code": "GSM1-ATLM", "name": "Atlantic"}, {"code": "BMNL-BMNL", "name": "Barablu Mobile"}, {"code": "BEN-SEAL", "name": "Ben"}, {"code": "BEPP-BEPP", "name": "Ben Prepaid"}, {"code": "LTEL-BLYK", "name": "Blyk"}, {"code": "TLFM-MECS", "name": "Call 4 Care"}, {"code": "INMO-CTEL", "name": "Citytel"}, {"code": "ETMB-CLUB", "name": "Club Mobiel"}, {"code": "TLFM-MECS", "name": "Comfour"}, {"code": "TLFM-MECS", "name": "Dailycom"}, {"code": "GSM1-DOPP", "name": "Data Only Prepaid"}, {"code": "TLNM-DEMO", "name": "DekaMarkt Mobiel"}, {"code": "GSM1-DKT", "name": "Dekatel"}, {"code": "TLNM-DBDM", "name": "Dirk Bas Digros Mobiel"}, {"code": "TLNM-EMOB", "name": "E Mobiel"}, {"code": "TLNM-DOBR", "name": "Dobrytel"}, {"code": "ETMB-ECOF", "name": "Ecofoon"}, {"code": "TLNM-EILI", "name": "Eili"}, {"code": "ETMB-ETMB", "name": "ElephantTalk"}, {"code": "TLNM-ESPM", "name": "Esprit Telecom"}, {"code": "GSM1-EUPH", "name": "Euphony"}, {"code": "LYCA-GTMO", "name": "GT Mobile"}, {"code": "GSM1-GLXY", "name": "Galaxy Business Networks B.V."}, {"code": "GSM1-PUMA", "name": "Hema"}, {"code": "TLNM-HLNW", "name": "Hollandsenieuwe"}, {"code": "ICMC-ICMC", "name": "Intercity"}, {"code": "GSM1-ICMC", "name": "Intercity (KPN)"}, {"code": "LTEL-ICMC", "name": "Intercity (Vodafone)"}, {"code": "GSM1-JMOB", "name": "Jumbo Mobiel"}, {"code": "GSM1-SPM", "name": "KPN"}, {"code": "GSM1-SPM", "name": "MTV"}, {"code": "GSM1-SPM", "name": "Hi"}, {"code": "INMO-KVMO", "name": "Kruidvat Mobiel"}, {"code": "GSM1-KVMO", "name": "Kruidvat Mobiel (KPN)"}, {"code": "ETMB-LIVV", "name": "LIVV"}, {"code": "UNIF-UNIF", "name": "Lancelot Telecom BV"}, {"code": "GSM1-LBRA", "name": "Lebara"}, {"code": "LYCA-LYCA", "name": "Lyca mobile"}, {"code": "TLFM-MECS", "name": "MEC Solutions"}, {"code": "GSM1-MEDI", "name": "Medion Mobile (AldiTalk)"}, {"code": "GSM1-MOBI", "name": "Mobile Service"}, {"code": "GSM1-MOVI", "name": "Mobile Vikings"}, {"code": "BMNL-BMNL", "name": "Mundio"}, {"code": "BEN-ARTA", "name": "NLD Mobiel (NLD Telecom)"}, {"code": "BEN-ARTA", "name": "NarrowMinds Mobile"}, {"code": "GSM1-ORTL", "name": "Ortel"}, {"code": "GSM1-POMB", "name": "PostNL Mobiel"}, {"code": "GSM1-SPVM", "name": "Rabo Mobiel"}, {"code": "GSM1-LIMO", "name": "Robin Mobile"}, {"code": "GSM1-ROUT", "name": "RoutIT B.V"}, {"code": "BEN-SCAR", "name": "Scarlet"}, {"code": "GSM1-SPM", "name": "Service Provider Mobiel"}, {"code": "BEN-EASY", "name": "Simpel"}, {"code": "GSM1-SMNL", "name": "Simyo"}, {"code": "TLFM-MECS", "name": "Smile"}, {"code": "GSM1-SOLM", "name": "Solcon Mobiel"}, {"code": "ETMB-SPMB", "name": "SpeakUp Mobile"}, {"code": "ETMB-SURA", "name": "Sura Mobile"}, {"code": "GSM1-SYMP", "name": "Sympac"}, {"code": "BEN-BEN", "name": "T-Mobile"}, {"code": "TEL2-TEL2", "name": "Tele2"}, {"code": "TEL2-TL2Z", "name": "Tele2 Zakelijk"}, {"code": "TLNM-TLNM", "name": "Teleena"}, {"code": "TLFM-MECS", "name": "Telefoon Totaal"}, {"code": "TLNM-TLSR", "name": "Telesur"}, {"code": "TLFM-TLFM", "name": "Telfort"}, {"code": "LYCA-TOGG", "name": "Toggle Mobile"}, {"code": "TLNM-TRND", "name": "TrendCall"}, {"code": "LTEL-TRU", "name": "Truphone"}, {"code": "ETMB-UWIS", "name": "U-Wiss Telecom"}, {"code": "TLNM-UMOB", "name": "UPC Mobiel"}, {"code": "UNIF-UNIF", "name": "Unify Mobile / Private Mobility"}, {"code": "TLNM-ONFO", "name": "Vast Mobiel"}, {"code": "TLFM-VSTM", "name": "Versatel Mobiel"}, {"code": "TLFM-MECS", "name": "VipTel"}, {"code": "LTEL-LIFN", "name": "Vodafone Abonnement"}, {"code": "LTEL-LBTP", "name": "Vodafone Prepaid"}, {"code": "GSM1-VMOB", "name": "Vvaa Mobiel"}, {"code": "GSM1-YEST", "name": "Yes Telecom"}, {"code": "TLFM-MECS", "name": "Yiggers"}, {"code": "GSM1-YOUF", "name": "Youfone"}, {"code": "ETMB-YPNI", "name": "Youpinie"}, {"code": "ETMB-YOUR", "name": "Your Card BV"}]
            }
        },
        "porting": { "post": {} },
        "profile/credit/history": {
            "get": {
                current_saldo: '13,23',
                items: [
                  {date:'4 uur geleden',amount:'-1:00', description: 'Aan Jesse gegeven'},
                  {date:'20 tot 25 april',amount:'-2,50', description: '*bliep aan'},
                  {date:'7 dagen geleden',amount:'10:00', description: 'Opwaardering'},
                  {date:'8 dagen geleden',amount:'-1:00', description: 'Aan Damiet gegeven'},
                  {date:'11 tot 15 april',amount:'-2:00', description: '*bliep aan'},
                  {date:'1 maand geleden',amount:'20:00', description: 'Opwaardering'},
                  {date:'1 maand geleden',amount:'-1:00', description: 'Tegoed aan Jesse gegeven'},
                  {date:'20 tot 25 maart',amount:'-2:50', description: '*bliep aan'},
                  {date:'2 maand geleden',amount:'20:00', description: 'Opwaardering'}
                ]
            }
        },
        "profile/credit/share": { "post": {} },
        "profile/credit/share/addressbook": {
            "get": [['Joost','1234511231'], ['Damiet','1947788230']]
        },
        "profile/credit/web": {
            "post": {"redirect_url": "https://live.adyen.com/hpp/pay.shtml" }
        },
        "profile/credit/voucher": {"post": {} },
        "profile/call/history": {
            "get": {
                'current_saldo': {"seconds": "21", "minutes": 34},
                'items': [
                  {date:'4 uur geleden',amount:'-2:30', description: 'Some User'},
                  {date:'10 dagen geleden',amount:'-1:21', description: '06 01010101'},
                  {date:'2 week geleden',amount:'10:00', description: 'Gratis minuten'},
                  {date:'3 week geleden',amount:'-6:12', description: 'Some User'}
                ]
            }
        },
        "profile/settings": {
            "get": {
              "voicemail_active": true,
              "call_from_balance": true,
              "signature_use": true
            },
            "put": {}
        },
        "profile/settings/roaming": {
            "get": {
              "free_eu_internet": true,
              "free_eu_use": false,
              "week_internet_bundle": true,
              "day_internet_bundle": true,
              "sms_bundle": true,
              "balance": "8217,00"
            },
            "put": {}
        },
        "profile/settings/recurring_payment": {
            "get": {
                "monthly": {
                    "amount": 10,
                    "enabled": true
                },
                "lowcredit": {
                    "amount": 0,
                    "enabled": false
                },
                "contract":"active"
            },
            "put": { },
            "delete": { }
        },
        "profile": {
            "get": {
                "today_paid_for": {
                    "on": true,
                    "plus": true,
                    "voice": false
                },
                "tariff": {
                    "on": true,
                    "plus": true,
                    "voice": false
                },
                "calltime": {
                    "minutes": "10",
                    "seconds": "5",
                },
                "name": "tester",
                "signature": "tester",
                "email": "tester@example.com"
            }
        },
        "profile/forgot_login": { "post": {} },
        "profile/referral": {
            "get": {
                'clicks':32,
                'credit':'12,50',
                'referral':'https://www.bliep.nl/r/000000000000'
            }
        },
        "profile/signature": {
            "get": {"signature": "*bliep"},
            "put": {}
        },
        "help/faq": {
            "get": {
                "faqs": [
                    {"category": 2, "answer": "Log in en klik...", "question": "Hoe zet ik *bliep aan?", "id": 1, "keywords": "service"},
                    {"category": 3, "answer": "Log in en klik...", "question": "Hoe zet ik mijn nummer over?", "id": 2, "keywords": "porting"}
            ]}
        },
        "help/search": {
            "get": []
        },
        "check_email": {
            "get": { 'email_exists': true}
        },
        "check_postcode": {
            "get": {
                'cityname': 'Amsterdam',
                'streetname': 'Nieuwendijk',
                'disable_ps_validation': false,
            }
        }
    };

    $.extend({
        BliepClient: BliepClient
    });
}));