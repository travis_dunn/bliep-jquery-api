module('api client');

function mockConsole() {
  console = { log: function() { ok(true, 'should print log') }};
}

function mockAjax(status) {
  status = status || 'success'
  $.ajax = function(options) {
    var def = new $.Deferred();
    def.resolve({status: status})
    return def;
  }
}

test('BliepClient()', function(){
  var options = {
    debug: true,
    test: true,
    expires: 90,
    baseUrl: 'localhost',
    clientId: 'test',
    done: function() {},
    fail: function() {}
  };

  var client = new $.BliepClient(options);
  equal(client.settings.debug, options.debug, 'should set debug option')
  equal(client.settings.test, options.test, 'should set test option')
  equal(client.settings.expires, options.expires, 'should set expires option')
  equal(client.settings.baseUrl, options.baseUrl, 'should set base URL option')
  equal(client.settings.clientId, options.clientId, 'should set client ID option')
  equal(client.settings.done, options.done, 'should set done callback option')
  equal(client.settings.fail, options.fail, 'should set fail callback option')
  equal(client.accessToken, '', 'should initialize empty access token')
  equal(client.clientAccessToken, '', 'should initialize empty client access token')
  equal(JSON.stringify(client.ajaxOptions), '{}', 'should initialize empty client ajax options hash')

  equal(client.ready.state(), 'pending', 'should initialize with pending ready state')
});

test('log ignored', function(){
  mockConsole();
  expect(0);
  var client = new $.BliepClient();
  client.log('test');
});

test('log printed', function(){
  mockConsole();    
  expect(1);
  var client = new $.BliepClient();
  client.settings.debug = true
  client.log('test');
});

test('isUserAuthenticated', function(){
  var client = new $.BliepClient();
  equal(client.isUserAuthenticated(), false, 'authentication should be false without access token');

  client.accessToken = 'test';
  equal(client.isUserAuthenticated(), true, 'authentication should be true with access token');
});

test('isClientAuthenticated', function(){
  var client = new $.BliepClient();
  equal(client.isClientAuthenticated(), false, 'authentication should be false without client access token');

  client.clientAccessToken = 'test';
  equal(client.isClientAuthenticated(), true, 'authentication should be true with client access token');
});

test('requestOptions', function(){
  var client = new $.BliepClient();
  client.clientAccessToken = 'test';
  var requestOptions = client.requestOptions('data')
  var headers = {
    'Authorization' : 'Bearer test',
    'X-Requested-With': 'XMLHttpRequest'
  }

  equal(requestOptions.beforeSend+'', 'function (){ self.beforeRequest(); }', 'should assign before request callback to before send hook')
  equal(requestOptions.complete+'', 'function (){ self.afterRequest(); }', 'should assign after request callback to complete hook')
  equal(requestOptions.data, 'data', 'should set data option')
  equal(requestOptions.dataType, 'json', 'should set data type to json')
  equal(requestOptions.contentType, 'application/json', 'should set content type to application/json')
  equal(JSON.stringify(requestOptions.headers), JSON.stringify(headers), 'should set oauth2 authentication header with token');

  client.ajaxOptions.async = false
  equal(client.requestOptions('data').async, false, 'should extend request options with ajaxOptions property overrides')
});

test('request logging', function(){
  mockConsole();    
  var client = new $.BliepClient();
  client.settings.debug = true
  expect(2);
  var res = client.request();
});

asyncTest('request network error', 1, function(){
  var client = new $.BliepClient();
  res = client.request({type: 'get', url: 'fail'})
  res.fail( function(data) {
    ok(true, 'should fail promise with bad request');
    start();
  });
});

asyncTest('request done', 2, function(){
  var client = new $.BliepClient();
  client.settings.done = function() {
    ok(true, 'should resolve promise for client done callback');
  }
  mockAjax()
  res = client.request({type: 'get', url: '127.0.0.1'})
  res.done( function(data) {
    equal(data.status, 'success', 'should resolve request promise with success status')
    start();
  });
});

asyncTest('request fail', 2, function(){
  var client = new $.BliepClient();
  client.settings.fail = function() {
    ok(true, 'should resolve promise for client fail callback');
  }
  mockAjax('fail')
  res = client.request({type: 'get', url: '127.0.0.1'})
  res.fail( function(data) {
    equal(data.status, 'fail', 'should reject request promise with fail status')
    start();
  });
});

asyncTest('request error', 2, function(){
  var client = new $.BliepClient();
  client.settings.fail = function() {
    ok(true, 'should resolve promise for client fail callback');
  }
  mockAjax('error')
  res = client.request({type: 'get', url: '127.0.0.1'})
  res.fail( function(data) {
    equal(data.status, 'error', 'should reject request promise with error status')
    start();
  });
});

test('get', 3, function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1'});
  client.request = function(options) {
    equal(options.data, 'data', 'should assign data params')
    equal(options.type, 'GET', 'should set get method')
    equal(options.url, '127.0.0.1/test', 'should build url from path and base')
  }
  client.get('/test', 'data');
});

test('post', 3, function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1'});
  var data = {'etawt':'et'};
  client.request = function(options) {
    var json = JSON.stringify(data)
    equal(options.data, json, 'should assign stringified json data')
    equal(options.type, 'POST', 'should set post method')
    equal(options.url, '127.0.0.1/test', 'should build url from path and base')
  }
  client.post('/test', data);
});

test('put', 3, function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1'});
  var data = {'data':'data'};
  client.request = function(options) {
    var json = JSON.stringify(data)
    equal(options.data, json, 'should assign stringified json data')
    equal(options.type, 'PUT', 'should set put method')
    equal(options.url, '127.0.0.1/test', 'should build url from path and base')
  }
  client.put('/test', data);
});

test('logout', 3, function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1'});
  client.accessToken = 'test'
  client.storage = {
    delete: function(key) {
      ok(true, 'should delete key "'+key+'"')
    }
  }
  client.logout()
  equal(client.accessToken, null, 'should set member access token to null')

});