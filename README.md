# Bliep jQuery Client Library

## Dependencies

+ jQuery 2+
+ jQuery Cookie Plugin (optional)

### Setup

There are two ways to setup the API client library. First, by including it as a script source in a page with access to the global jQuery `$` function alias.

    <script type='text/javascript' src='bliep-jquery-api.js'></script>

Second, using the RequireJS style.

    require ('/bliep-jquery-api');

In some cases it may be preferable to load the library in a RequireJS configuration.

    requirejs.config( {
        "shim": {
            "bliep-jquery-api" : ["jquery"]
        }
    });

To use the API client, initialize an instance with your client's OAuth consumer id. Pass the `authenticate` option to immediately authenticate your client for making public API calls (e.g. for anonymous users).

    var client = new $.BliepClient({ clientId: 'xxxxxxxxxxxxx', authenticate: true });

To print request/response information to the console, intialize the client with the `debug` option. To mock all network requests (useful for local testing) and immediately return a response, pass the `test` option.

    var client = new $.BliepClient({ clientId: 'xxxxxxxxxxxxx', debug: true, test: true });

To configure session expiration (for cookie-based storage), intilize the client with the `expires` option set to the number of days after which the API access tokens should expire. By default, the session ends when the browser is closed.

    var client = new $.BliepClient({ clientId: 'xxxxxxxxxxxxx', expires: 7 });

### Waiting for Authorization

Because network issues can cause the API authorization to be arbitrarily delayed, you can use the client's `ready` method to return a promise that waits until a token has been retrieved before invoking dependent code.

    client.ready.done(function(){
        client.someAPICall(); // Succeeds! Promise resolved after authenticateClient() gets token.
    })
    client.someAPICall(); // Fails! called before the client has a token.

### Testing

The library inclues a suite of tests written in QUnit. To run the tests, open the `./tests/index.html` file in a browser or headless browser test runner.

### Useage

The API client provides a number of RPC functions that call the *bliep API. These functions are implemented as jQuery AJAX requests and therefore return a deferred object to which you should bind your code on the `done`, `fail`, or `always` hooks.

#### orderAndRegister ####

This method registers a new user account and orders a SIM card for delivery to the user's address. The fullfillment of the order is handled by an offsite merchant gateway and so after a successful request the user should be redirected to a payment URL to complete the process.

    client.orderAndRegister('my name','new@example.com','pass123','M',date.getTime(),false,'my st.','7a','my city','1010AB','NORMAL')
          .done(function(data, textStatus){
            console.log(data.email_exists)
          })

###### Parameters

+ name (required): username in *bliep
+ email (required): a valid, previously unregistered email address
+ password (required): user *bliep login password
+ sex (optional): gender, can be `M` or `F`
+ number_porting (required): does order include a number porting request, can be `true` or `false`
+ street_name (required): the street address where the card should be delivered
+ house_number (required): the house number where the card should be delivered
+ city_name (required): the city where the card should be delivered
+ postal_code (required): the postal code where the card should be delivered
+ simtype (required): the type of SIM card , can be `NORMAL` or `NANO`
+ redirect_url (required): the page the user returns to after completing payment
+ referral_code (optional): a *bliep user's unique referral code

###### Response

    {"status": "success", "data": {
      'redirect_url': "https://test.adyen.com/hpp/pay.shtml?"
    }}

#### listFAQ ####

This method lists all the currently highlighted *bliep FAQ.

    client.listFAQ(127001)
          .done(function(data, textStatus){
            console.log()
          })

###### Parameters

+ category_id (optional): the FAQ category ID by which to filter the results. The following IDs are supported:

      Personal Data = 72007
      Credit History = 73004
      Signature = 74007
      Parents = 75006
      General = 123001
      Ordering = 124001
      Upgrading = 125001
      Sharing = 126001
      Self Care = 127001
      Number Porting = 193036

###### Response

    {"status": "success", "data": {"faqs": [
        {"category": 2, "answer": "Log in en klik...", "question": "Hoe zet ik *bliep aan?", "id": 1, "keywords": "service"},
        {"category": 3, "answer": "Log in en klik...", "question": "Hoe zet ik mijn nummer over?", "id": 2, "keywords": "porting"},
        
    ]}}

#### searchFAQ ####

This method returns FAQ matching the given keyword search input. Note, the search itself is quite literal and doesn't support wildcards.

    client.searchFAQ('buitenland')
          .done(function(data, textStatus){
            console.log()
          })

###### Parameters

+ text (required): an alphanumeric string of keywords to find in the FAQ.

###### Response

    {"status": "success", "data": {"faqs": [
        {"category": 2, "answer": "Log in en klik...", "question": "Hoe zet ik *bliep aan?", "id": 1, "keywords": "service"},
        {"category": 3, "answer": "Log in en klik...", "question": "Hoe zet ik mijn nummer over?", "id": 2, "keywords": "porting"},
        
    ]}}  

#### askQuestion ####

This method submits a question to the *bliep support team using a given email.

    client.askQuestion('bliep', 'bliep@example.com', 'Is the *bliep javascript API public?')
          .done(function(data, textStatus){
            console.log()
          })

###### Parameters

+ name (required): the name of the person asking the question
+ email (required): a valid email address for the person asking a question; used for follow-up coorespondence
+ message (required): a plaintext message containing a question.

###### Response

    {"status": "success", "data": null }}      

#### checkEmail ####

This method checks if a given email exists and is intended for use with, for example, form validations.

    client.checkEmail('bliep@example.com')
          .done(function(data, textStatus){
            console.log(data.email_exists)
          })

###### Parameters

+ email (required): a valid email address to lookup

###### Response

    {"status": "success", "data": {'email_exists': false} }}